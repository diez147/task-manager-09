package ru.tsc.babeshko.tm.api;

import ru.tsc.babeshko.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
