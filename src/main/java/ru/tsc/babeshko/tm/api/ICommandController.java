package ru.tsc.babeshko.tm.api;

public interface ICommandController {
    void showSystemInfo();

    void showErrorArgument(String arg);

    void showErrorCommand(String arg);

    void showWelcome();

    void showHelp();

    void showCommands();

    void showArguments();

    void showVersion();

    void showAbout();

}
