package ru.tsc.babeshko.tm.service;

import ru.tsc.babeshko.tm.api.ICommandRepository;
import ru.tsc.babeshko.tm.model.Command;

public class CommandService implements ru.tsc.babeshko.tm.api.ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
